package net.DeltaWings.RasberryPi.Gpio;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Menu extends JDialog {
	private JPanel contentPane;
	private JTextField textField1;
	private JButton setPinButton;
	private JTextField PinMode;
	private JButton setModeButton;
	private JTextField ValueTextField;
	private JButton mettreValeurButton;
	private JButton recevoirValeurButton;
	private Integer pin;
	private String mode;
	private Menu() {
		setTitle("Contrôle du GPIO");
		setContentPane(contentPane);
		setModal(true);
		setPinButton.addActionListener(e -> {
			if(isInteger(textField1.getText())) {
				pin = Integer.parseInt(textField1.getText());
				result("Pin séléctionné changé");
			} else result("Error Pin");
		});
		setModeButton.addActionListener(e -> {
			mode = PinMode.getText();
			if(mode.equalsIgnoreCase("in") || mode.equalsIgnoreCase("out")) {
				try {
					result(getCmd("setmode.sh", pin.toString(), mode));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else result("Error");
		});
		recevoirValeurButton.addActionListener(e -> {
			try {
				ValueTextField.setText(getCmd( "getval.sh", pin.toString(), ""));
			} catch (IOException e1) {
				e1.printStackTrace();
			} result("Done");
		});
		mettreValeurButton.addActionListener(e -> {
			if(isInteger(ValueTextField.getText())) {
				try {
					result(getCmd("setval.sh", pin.toString(), ValueTextField.getText() ));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else result("Error");
		});
	}
	private static void c() throws IOException {
		System.out.print("> ");
		Scanner i = new Scanner(System.in);
		String[] l = i.nextLine().split(" ");
		if(l.length == 1) {
			if(l[0].equalsIgnoreCase("help")) out("      GPIO Help\n-----------------------\nNo return means Error in syntax\n-----------------------\nexit to finish\nmode<physical pin> <mode(out or in)>\nget <physical pin>\nsend <physical pin> <value>\n-----------------------\nMade with love by Aviortheking\nSource at bitbucket.org/delta-wings/gpiocontrol/");
			else if(l[0].equalsIgnoreCase("exit")) System.exit(0);
		} else if(l.length == 2 && l[0].equalsIgnoreCase("get") && isInteger(l[1])) {
			out("Getting Value...");
			out("Value : " + getCmd("getval.sh", l[1], ""));
		} else if(l.length == 3) {
			if(l[0].equalsIgnoreCase("mode") && isInteger(l[1])) {
				if(l[2].equalsIgnoreCase("in") || l[2].equalsIgnoreCase("out")) {
					getCmd("setmode.sh", l[1], l[2]);
					out("Mode set to " + l[2] + " !");
				}
			} else if(l[0].equalsIgnoreCase("set") && isInteger(l[1]) && isInteger(l[2])) {
				getCmd("setvalue.sh", l[1], l[2]);
				out("Value set !");
			}
		} c();
	}

	public static void main(String[] a) throws IOException {
		if(a.length == 1 && a[0].equalsIgnoreCase("nogui")) {
			out("Launched ! use help to get help");
			c();
		} else {
			Menu d = new Menu();
			d.pack();
			d.setVisible(true);
			System.exit(0);
		}
	}
	private static void out(Object t) {
		System.out.println(t);
	}
	private void result(String t) {
		Result d = new Result(t);
		d.pack();
		d.setVisible(true);
	}
	private static String getCmd(String s, String va, String vb) throws IOException {
		String[] c = {Menu.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("GPIOControl.jar", "") + "scripts/" + s, va, vb};
		Process p = Runtime.getRuntime().exec(c);
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String l = r.readLine();
		if(l != null) return l;
		else return "Erreur";
	}
	private static boolean isInteger(String s) {
		if (s.isEmpty()) return false;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1) return false;
				else continue;
			} if (Character.digit(s.charAt(i), 10) < 0) return false;
		} return true;
	}
}